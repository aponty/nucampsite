import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Card } from 'react-native-elements';
import { CAMPSITES } from '../shared/campsites';

function RenderCampsite({campsite}) {
    if (campsite) {
        return (
            <Card
                featuredTitle={campsite.name}
                image={require('./images/react-lake.jpg')}>
                <Text style={{margin: 10}}>
                    {campsite.description}
                </Text>
            </Card>
        );
    }
    return <View />;
}

class CampsiteInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            campsites: CAMPSITES
        }
    }

    static navigationOptions = {
        title: "Campsite Information"
    }

    render(){
        const campsiteID = this.props.navigation.getParam('campsiteID');
        const campsite = this.state.campsites.filter(camp => camp.id === campsiteID)[0];
        return <RenderCampsite campsite={campsite} />;
    }
}

export default CampsiteInfo;